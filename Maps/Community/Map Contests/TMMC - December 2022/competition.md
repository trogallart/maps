# December 2022 Map Making Competition:

### Rules of the competition:

1. Send your creation on our discord in our cartography channel before the 10.
2. Your map shall be no bigger than 96 by 96 tiles.
3. Your map has to feature either 3 or 5 active players.
4. Your map should be made playable using the melee and one peon only starting conditions. All of the default starting resources should also be playable.
5. No red or black player shall be present on the map by default.
6. Your map should use the default units and upgrades values.
7. There should be at least one critter on the map.

### Useful links:
[Trog's twitch channel](https://www.twitch.tv/trogallart)
[Our amazing Discord](https://discord.gg/ADt3TgFuV7)
[Twitter](https://twitter.com/trogallart)

### Contestant's submissions first observation on the 17 morning:

* Treehann : Map is 128x128 tiles big.
* Thejunior : Map displays 4 players when hosted melee

### 
