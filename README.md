# README #

This repository contains the last saved state of my Warcraft 2 install directory with extras.
This repo includes my Maps folder with all the maps sorted.
It also includes my ddraw configuration.

### What is this repository for? ###

* This repo contains  most of the maps we play on GOG and will be updated regularly.

### How do I get set up? ###

* Open your Warcraft 2 game directory (the one containing the .exe of the game).
* git clone the repository in your game's folder and voila.
* Enjoy the sorted maps folder and all the cool stuff.

### Who do I talk to? ###

* For any request concerning this repository contact me on Discord: https://discord.gg/ADt3TgFuV7
* If you want to submit your maps that also happens on discord.
